function layThongTinTuForm(){
    var tk=document.getElementById("tknv").value;
    var ten=document.getElementById("name").value;
    var email=document.getElementById("email").value;
    var ngayLam=document.getElementById("datepicker").value;
    var luongCB=document.getElementById("luongCB").value*1;
    // 
    var chucVu=document.getElementById("chucvu");
    var opLuong=chucVu.options[chucVu.selectedIndex].text;
    var opLuong2=chucVu.options[chucVu.selectedIndex].value;
    // 
    var gioLam=document.getElementById("gioLam").value;

// tổng lương
    var tongLuong="";
    if (opLuong2==1){
        tongLuong=luongCB*3;
    } if (opLuong2==2){
        tongLuong=luongCB*2;
    }if (opLuong2==3){
        tongLuong=luongCB;
    }

// xếp loại
    var xepLoai="";
    if (gioLam>=192){
        xepLoai="nhân viên xuất sắc";
    }else if (gioLam>=176){
        xepLoai="nhân viên giỏi";
    }else if (gioLam>=160){
        xepLoai="nhân viên khá";
    }else {
        xepLoai="nhân viên trung bình";
    }

    var nv= new nhanVien(tk, ten, email, ngayLam, opLuong,tongLuong, xepLoai)

    return nv;
}

function renderDsnv(nvArr){ 
    
    var contentHTML="";

    for (var index=0; index<nvArr.length; index++){
        var currentNv=nvArr[index];
        var danhSachTr=`<tr>
        <td>${currentNv.taikhoan}</td>
        <td>${currentNv.hoten}</td>
        <td>${currentNv.email}</td>
        <td>${currentNv.ngaylam}</td>
        <td>${currentNv.chucvu}</td>
        <td>${currentNv.tongluong}</td>
        <td>${currentNv.xeploai}</td>
        </tr>`

        contentHTML+=danhSachTr;

    }document.getElementById("tableDanhSach").innerHTML=contentHTML;
}